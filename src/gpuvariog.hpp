#include <Rcpp.h>
using namespace Rcpp;

RcppExport
SEXP variogcpp(const SEXP coords, const SEXP data,
               const SEXP low, const SEXP high,
               const SEXP incr, const SEXP gpuid);
