/////////////////////////////////////////////////////
// Name: GpuVariogram.cpp			   //
// Created: 2015-08-06				   //
// Author: Chris Jewell <c.jewell@lancaster.ac.uk> //
// Copyright: Chris Jewell 2015                    //
// Purpose: CUDA-implemented function to calculate //
//           an empirical semivariogram.	   //
/////////////////////////////////////////////////////

#include <iostream>
#include <cuda_runtime.h>
#include <stdexcept>
#include <cmath>
#include <cstdio>

#include "KernelUtils.cuh"
#include "GpuVariogram.hpp"


inline
__device__
float
distance(Point2D x1, Point2D x2)
{
  float dx = x1.x - x2.x;
  float dy = x1.y - x2.y;
  return hypotf(dx, dy);
}

__global__
void
kSemivariogram(const float* z, const Point2D* points, const int n,
	       const int low, const int high, const int incr,
	       float* dSS, int* dN)
{
  int col = threadIdx.x + blockIdx.x * blockDim.x;
  int row = threadIdx.x + blockIdx.y * blockDim.x;

  extern __shared__ char buff[];
  Point2D* pointbuff = (Point2D*)buff;
  int offset = blockDim.x*sizeof(Point2D);
  float* zbuff = (float*)(buff + offset);
  offset += blockDim.x*sizeof(float); // Naturally aligned
  float* SS = (float*)(buff + offset);
  offset += blockDim.x*sizeof(float);
  int* N = (int*)(buff + offset);                        // avoid bank conflicts (I hope!)

  SS[threadIdx.x] = 0.f;
  N[threadIdx.x] = 0.f;

  // Read in rows
  if(row < n)
    {
      pointbuff[threadIdx.x] = points[row];
      zbuff[threadIdx.x] = z[row];
    }
  __syncthreads();

  // Read in cols
  if(col < n)
    {
      Point2D x1 = points[col];
      float zcol = z[col];
      int rowlimit = min(blockDim.x, n - blockIdx.y*blockDim.x);
      for(int myrow = 0; myrow < rowlimit; myrow++)
	{
	  Point2D x2 = pointbuff[myrow];
	  float d = distance(x1, x2);

	  // Calculate variogram
	  for(int h=low, pos=0; h<high; h+=incr, pos++)
	    {
	      SS[threadIdx.x] = 0.f;
	      N[threadIdx.x] = 0;

	      if((h < d) & (d <= (h+incr)))
		{
		  float zdiff = zbuff[myrow] - zcol;
		  SS[threadIdx.x] += zdiff*zdiff;
		  N[threadIdx.x]++;
		}
	      _shmemReduce(SS);
	      _shmemReduce(N);

	      if(threadIdx.x == 0) {
		atomicAdd(dSS+pos, SS[0]);
		atomicAdd(dN+pos, N[0]);
		//dSS[blockIdx.x + pos*gridDim.x] = SS[0];
		//dN[blockIdx.x + pos*gridDim.x] = N[0];
	      }
	    }

	}

    }
}



std::vector<float>
GPUVariogram(const std::vector<float> Z, const Points2D points,
	      const int min, const int max, const int incr,
	      const int blockSize, const int gpuid)
{

  if(Z.size() != points.size()) throw std::runtime_error("Z and points not of same length");

  // Upload data to GPU
  float* dZ;
  Point2D* dPoints;
  int n = Z.size();
  checkCudaError(cudaMalloc(&dZ, n*sizeof(float)));
  checkCudaError(cudaMemcpy(dZ, Z.data(), n*sizeof(float), cudaMemcpyHostToDevice));
  checkCudaError(cudaMalloc(&dPoints, n*sizeof(Point2D)));
  checkCudaError(cudaMemcpy(dPoints, points.data(), n*sizeof(Point2D), cudaMemcpyHostToDevice));

  // Configure kernel
  dim3 blockDim(blockSize, 1);
  dim3 gridDim((n + blockSize - 1)/blockSize,
	       (n + blockSize - 1)/blockSize);

  // Result vectors
  int resultSize = (max - min)/incr + 1;
  float* dSS;
  int* dN;
  checkCudaError(cudaMalloc(&dSS, resultSize*sizeof(float)));
  checkCudaError(cudaMalloc(&dN, resultSize*sizeof(int)));
  checkCudaError(cudaMemset(dSS, 0, resultSize*sizeof(float)));
  checkCudaError(cudaMemset(dN, 0, resultSize*sizeof(float)));

  // Launch the kernel
  int shmem = blockSize * (sizeof(float) + sizeof(Point2D) + sizeof(float) + sizeof(int));

  try {
    kSemivariogram<<<gridDim, blockDim, shmem>>>(dZ, dPoints, n, min, max, incr, dSS, dN);
    checkCudaError(cudaDeviceSynchronize());
  }
  catch(GpuRuntimeError& e) {
    cudaDeviceReset();
    throw e;
  }

  // Grab back results and process
  std::vector<float> hSS(resultSize);
  std::vector<int> hN(resultSize);
  checkCudaError(cudaMemcpy(hSS.data(), dSS, resultSize*sizeof(float), cudaMemcpyDeviceToHost));
  checkCudaError(cudaMemcpy(hN.data(), dN, resultSize*sizeof(int), cudaMemcpyDeviceToHost));
  for(int i=0; i<resultSize; ++i)
    {
      hSS[i] /= 2*hN[i];
    }

  checkCudaError(cudaFree(dZ));
  checkCudaError(cudaFree(dPoints));
  checkCudaError(cudaFree(dSS));
  checkCudaError(cudaFree(dN));

  cudaDeviceReset();

  return hSS;
}

void
resetGPU()
{
  cudaDeviceReset();
}
