/////////////////////////////////////////////////////
// Name: Semivariogram.hpp			   //
// Created: 2015-08-06				   //
// Author: Chris Jewell <c.jewell@lancaster.ac.uk> //
// Purpose: CUDA-implemented function to calculate //
//           an empirical semivariogram.	   //
/////////////////////////////////////////////////////

#include <vector>

#ifndef GPUVARIOGRAM_H
#define GPUVARIOGRAM_H

struct Point2D
{
  float x;
  float y;
};

typedef std::vector<Point2D> Points2D;


/** /brief Calculates a variogram
 *
 *  \param Z a vector of values for which to calculate the variogram
 *  \param points the point locations at which to calculate the variogram
 *  \param min minimum distance
 *  \param max maximum distance
 *  \param incr increments for which to calculate values of the variogram
 *  \returns a vector of values of the variogram
 */
std::vector<float>
GPUVariogram(const std::vector<float> Z, const Points2D points, const int min, const int max, const int incr, const int blockSize=128, const int gpuid=-1);

void
resetGPU();

#endif
