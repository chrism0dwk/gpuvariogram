#include <Rcpp.h>
#include <vector>
#include <exception>

#include "gpuvariog.hpp"
#include "GpuVariogram.hpp"

using namespace Rcpp;

RcppExport
SEXP variogcpp(const SEXP coords, const SEXP data,
                    const SEXP low, const SEXP high,
                    const SEXP incr, const SEXP gpuid)
{

  DataFrame _coords(coords);
  NumericVector _data(data);
  NumericVector _low(low);
  NumericVector _high(high);
  NumericVector _incr(incr);
  NumericVector _gpuid(gpuid);

  NumericVector x = _coords[0];
  NumericVector y = _coords[1];

  std::vector<Point2D> points(x.size());
  std::vector<float> z(_data.size());
  for(int i=0; i<x.size(); ++i) {
    Point2D pt;
    pt.x = x[i];
    pt.y = y[i];
    points[i] = pt;
    z[i] = _data[i];
  }

  std::vector<float> v;
  try {
    v = GPUVariogram(z, points, _low[0], _high[0], _incr[0], 128, _gpuid[0]);
  }
  catch(std::exception& e) {
    Rcerr << "GPU Exception: " << e.what() << "\n";
    resetGPU();
  }
  NumericVector rv(v.size());
  for(int i=0; i<v.size(); ++i) rv[i] = v[i];

  return rv;
}

